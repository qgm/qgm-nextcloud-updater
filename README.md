# QGM Nextcloud Updater

A simple Script for Updating Nextcloud from the command line. Comes without any warranty, use at your own risk, dont use at all if you are not unterstanding what it does!

Discussion (german): https://ask.linuxmuster.net/t/update-nextcloud-13-05-14-0-macht-probleme/2685/15

## Installation

- cd /opt
- git clone https://gitlab.com/qgm/qgm-nextcloud-updater
- ln -s /opt/qgm-nextcloud-updater/qgm-nextcloud-update /usr/local/sbin/

Now you have the command qgm-nextcloud-update available as root.

## Update

- cd /opt/qgm-nextcloud-updater
- git pull

## Usage

- Get the newest Nextcloud Zip from https://nextcloud.com/install/
- Edit the script to adjust the config settings (or better: copy the config block to /etc/qgm-nextcloud-updater/config and adjust the settings there, so they will not be affected by future updates)
- Run qgm-nextcloud-update nextcloud-xx.x.x.zip 
- Check the summary
- Proceed by typing "y"
- Hope for the best


